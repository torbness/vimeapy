from cython_funcs import *
import unittest
import numpy as np
from MoI import MoI
import pylab as plt
import time

class TestMoI(unittest.TestCase):

    def test_homogeneous(self):
        """If saline and tissue has same conductivity, MoI formula
        should return 2*(inf homogeneous point source)."""
        set_up_parameters = {
            'sigma_G': 0.0, # Below electrode
            'sigma_T': 0.3, # Tissue
            'sigma_S': 0.3, # Saline
            'h': 200,
            'steps': 20}
        Moi = MoI(**set_up_parameters)
        imem = 1.2
        charge_pos = [0, 0, 100]
        elec_pos = [0, 0, 0]
        dist = np.sqrt(np.sum(np.array(charge_pos) - np.array(elec_pos))**2)
        expected_ans = 2/(4*np.pi*set_up_parameters['sigma_T']) * imem/dist
        returned_ans = Moi.isotropic_moi(charge_pos, elec_pos, imem)
        self.assertAlmostEqual(expected_ans, returned_ans, 6)

    def test_saline_effect(self):
        """ If saline conductivity is bigger than tissue conductivity, the
        value of 2*(inf homogeneous point source) should be bigger
        than value returned from MoI"""
        set_up_parameters = {
            'sigma_G': 0.0, # Below electrode
            'sigma_T': 0.3, # Tissue
            'sigma_S': 3.0, # Saline
            'h': 200,
            'steps': 20}
        Moi = MoI(**set_up_parameters)
        imem = 1.2
        charge_pos = [0, 0, 100]
        elec_pos = [0, 0, 0]
        dist = np.sqrt(np.sum((np.array(charge_pos) - np.array(elec_pos))**2))
        expected_ans = 2/(4*np.pi*set_up_parameters['sigma_T']) * imem/dist
        returned_ans = Moi.isotropic_moi(charge_pos, elec_pos, imem)
        self.assertGreater(expected_ans, returned_ans)

    def test_charge_closer(self):
        """ If charge is closer to electrode, the potential should
        be greater"""
        set_up_parameters = {
            'sigma_G': 0.0, # Below electrode
            'sigma_T': 0.3, # Tissue
            'sigma_S': 3.0, # Saline
            'h': 200,
            'steps': 20}
        Moi = MoI(**set_up_parameters)
        imem = 1.2
        charge_pos_1 = [0, 0, 10]
        charge_pos_2 = [0, 0, 5]
        elec_pos = [0, 0, 0]
        returned_ans_1 = Moi.isotropic_moi(charge_pos_1, elec_pos, imem)
        returned_ans_2 = Moi.isotropic_moi(charge_pos_2, elec_pos, imem)
        self.assertGreater(returned_ans_2, returned_ans_1)

    def test_within_domain_check(self):
        """ Test if unvalid electrode or charge position raises RuntimeError.
        """
        h = 200
        set_up_parameters = {
            'sigma_G': 0.0, # Below electrode
            'sigma_T': 0.3, # Tissue
            'sigma_S': 3.0, # Saline
            'h': h,
            'steps': 20}
        Moi = MoI(**set_up_parameters)

        invalid_positions = [[0, 0, 1000],
                            [0, 0, -1000],
                            [0, 0, -1]]
        valid_position = [0, 0, 100.]

        with self.assertRaises(RuntimeError):
            Moi.point_source_moi_at_elec_plane(valid_position, valid_position)
            for pos in invalid_positions:
                Moi.point_source_moi_at_elec_plane(valid_position, pos)
                Moi.point_source_moi_at_elec_plane(pos, valid_position)

        xstart = np.array([100., 0, -100., -110])
        xend = np.array([100., 0, -100., -110]) + 100.

        ystart = np.array([100., 0, -100, 200.])
        yend = np.array([100., 0, -100, 200]) + 100.

        zstart = np.array([-100, -10, 110, 150.])
        zend = np.array([100, 10, -110, -150]) + 10.

        xmid = np.array([100, 0, -100.])
        ymid = np.array([100, 0, -100.])
        zmid = np.array([10, 0, -10.])

        elec_x = (np.arange(3) - 1) * 50.
        elec_y = (np.arange(3) - 1) * 50.

        n_avrg_points = 1
        elec_r = 1
        with self.assertRaises(RuntimeError):
            mapping = LS_with_elec_mapping(set_up_parameters['sigma_T'], set_up_parameters['sigma_S'],
                                           h, set_up_parameters['steps'], n_avrg_points, elec_r, elec_x, elec_y,
                                           xstart, ystart, zstart, xend, yend, zend)
        with self.assertRaises(RuntimeError):
            mapping = LS_without_elec_mapping(set_up_parameters['sigma_T'], set_up_parameters['sigma_S'],
                                              h, set_up_parameters['steps'],  elec_x, elec_y, xstart, ystart, zstart,
                                              xend, yend, zend)

        with self.assertRaises(RuntimeError):
            mapping = PS_without_elec_mapping(set_up_parameters['sigma_T'], set_up_parameters['sigma_S'],
                                    h, set_up_parameters['steps'], elec_x, elec_y, xmid, ymid, zmid)

        with self.assertRaises(RuntimeError):
            mapping = PS_with_elec_mapping(set_up_parameters['sigma_T'], set_up_parameters['sigma_S'], h,
                                           set_up_parameters['steps'], n_avrg_points, elec_r, elec_x, elec_y,
                                           xmid, ymid, zmid)

        elec_z = -100
        zmid += 150
        zstart += 150
        zend += 150
        with self.assertRaises(RuntimeError):
            mapping = LS_with_elec_mapping(set_up_parameters['sigma_T'],
                                           set_up_parameters['sigma_S'],
                                           elec_z, set_up_parameters['steps'],
                                           n_avrg_points, elec_r, elec_x, elec_y, xstart, ystart, zstart,
                                           xend, yend, zend)
        with self.assertRaises(RuntimeError):
            mapping = LS_without_elec_mapping(set_up_parameters['sigma_T'],
                                              set_up_parameters['sigma_S'],
                                              elec_z, set_up_parameters['steps'],
                                              elec_x, elec_y, xstart, ystart, zstart,
                                              xend, yend, zend)

        with self.assertRaises(RuntimeError):
            mapping = PS_without_elec_mapping(set_up_parameters['sigma_T'],
            set_up_parameters['sigma_S'],
            elec_z, set_up_parameters['steps'],
            elec_x, elec_y, xmid, ymid, zmid)

        with self.assertRaises(RuntimeError):
            mapping = PS_with_elec_mapping(set_up_parameters['sigma_T'],
                                           set_up_parameters['sigma_S'], elec_z,
                                           set_up_parameters['steps'],
                                           n_avrg_points, elec_r, elec_x, elec_y,
                                           xmid, ymid, zmid)

    def test_if_anisotropic(self):
        """ Test if it can handle anisotropies
        """
        set_up_parameters = {
            'sigma_G': [1.0, 1.0, 1.0], # Below electrode
            'sigma_T': [0.1, 0.1, 1.0], # Tissue
            'sigma_S': [0.0, 0.0, 0.0], # Saline
            'h': 200,
            'steps': 2}
        Moi = MoI(**set_up_parameters)
        self.assertTrue(Moi.is_anisotropic)

        set_up_parameters = {
            'sigma_G': [1.0], # Below electrode
            'sigma_T': [1.0], # Tissue
            'sigma_S': [0.0], # Saline
            'h': 200,
            'steps': 2}
        with self.assertRaises(ValueError):
            moi = MoI(**set_up_parameters)

    def atest_moi_line_source(self):
        """ Testing infinite isotropic moi line source formula"""
        set_up_parameters = {
            'sigma_G': 0.0, # Below electrode
            'sigma_T': 0.3, # Tissue
            'sigma_S': 3.0, # Saline
            'h': 200,
            'steps': 20}
        Moi = MoI(**set_up_parameters)
        comp_start = [-50., -100., 140.]
        comp_end = [10., 100., 10.]
        comp_mid = (np.array(comp_end) + np.array(comp_start))/2

        elec_y = np.linspace(-150, 150, 25)
        elec_x = np.linspace(-150, 150, 25)
        phi_PS = []
        phi_PSi = []
        y = []
        x = []
        points = 40
        s = np.array(comp_end, dtype=float) - np.array(comp_start)
        ds = s / (points-1)
        for x_pos in xrange(len(elec_x)):
            for y_pos in xrange(len(elec_y)):
                phi_PS.append(Moi.isotropic_moi(comp_mid, [elec_x[x_pos], elec_y[y_pos], 0]))
                delta = 0
                for step in xrange(points):
                    pos = comp_start + ds * step
                    delta += Moi.isotropic_moi(pos, [elec_x[x_pos], elec_y[y_pos], 0], imem=1./(points+1))
                phi_PSi.append(delta)
                x.append(elec_x[x_pos])
                y.append(elec_y[y_pos])
        x = np.array(x)
        y = np.array(y)

        cyth = LS_without_elec_mapping(set_up_parameters['sigma_T'],
                                       set_up_parameters['sigma_S'],
                                       set_up_parameters['h'], set_up_parameters['steps'],
                                       x, y, np.array([comp_start[0]]),
                                       np.array([comp_start[1]]), np.array([comp_start[2]]),
                                       np.array([comp_end[0]]), np.array([comp_end[1]]), np.array([comp_end[2]]))

        phi_PSi = np.array(phi_PSi).reshape(len(elec_x), len(elec_y)) / np.max(cyth)
        phi_PS = np.array(phi_PS).reshape(len(elec_x), len(elec_y)) / np.max(cyth)
        cyth = cyth[:, 0].reshape(len(elec_x), len(elec_y)) / np.max(cyth)

        plt.subplot(221, frameon=False)
        plt.imshow(cyth, interpolation='nearest')
        plt.axis('equal')
        plt.colorbar()
        plt.subplot(222, frameon=False)
        plt.imshow(phi_PS, interpolation='nearest')
        plt.axis('equal')
        plt.colorbar()
        plt.subplot(223, frameon=False)
        plt.imshow(phi_PSi, interpolation='nearest')
        plt.axis('equal')
        plt.colorbar()
        plt.subplot(224, frameon=False)
        plt.imshow(cyth - phi_PSi, interpolation='nearest')
        plt.axis('equal')
        plt.colorbar()
        plt.savefig('line_source_test_cython.png')

    def atest_general_moi_formula_shift(self):
        for n in xrange(100):
            h = np.random.random() * 500.
            set_up_parameters = {
                             'sigma_G': np.random.random()*5.0, # Below electrode
                             'sigma_T': np.random.random()*5.0, # Tissue
                             'sigma_S': np.random.random()*5.0, # Saline
                             'h': h, # um
                             'steps': 20,
                              }
            moi = MoI(**set_up_parameters)

            xpos_charge = (np.random.random() - 0.5) * 100
            ypos_charge = (np.random.random() - 0.5) * 100
            zpos_charge = np.random.random() * h

            xpos_elec = (np.random.random() - 0.5) * 100
            ypos_elec = (np.random.random() - 0.5) * 100
            zpos_elec = 0

            charge_pos = [xpos_charge, ypos_charge, zpos_charge]
            elec_pos = [xpos_elec, ypos_elec, zpos_elec]
            charge_pos_shifted = [xpos_charge, ypos_charge, zpos_charge - h / 2.]
            elec_pos_shifted = [xpos_elec, ypos_elec, zpos_elec - h / 2.]

            orig = moi.isotropic_moi(charge_pos, elec_pos)
            shifted = moi.isotropic_moi_shifted(charge_pos_shifted, elec_pos_shifted)
            self.assertAlmostEqual(orig, shifted, 6)

    def atest_simplified_moi_formula_shifting(self):
        for n in xrange(100):
            h = np.random.random() * 500.
            set_up_parameters = {
                             'sigma_G': 0, # Below electrode
                             'sigma_T': np.random.random()*5.0, # Tissue
                             'sigma_S': np.random.random()*5.0, # Saline
                             'h': h, # um
                             'steps': 20,
                              }
            moi = MoI(**set_up_parameters)
            xpos_charge = (np.random.random() - 0.5) * 100
            ypos_charge = (np.random.random() - 0.5) * 100
            zpos_charge = np.random.random() * h

            xpos_elec = (np.random.random() - 0.5) * 100
            ypos_elec = (np.random.random() - 0.5) * 100
            zpos_elec = 0

            charge_pos = [xpos_charge, ypos_charge, zpos_charge]
            elec_pos = [xpos_elec, ypos_elec, zpos_elec]

            charge_pos_shifted = [xpos_charge, ypos_charge, zpos_charge - h / 2.]
            elec_pos_shifted = [xpos_elec, ypos_elec, zpos_elec - h / 2.]

            orig = moi.point_source_moi_at_elec_plane(charge_pos, elec_pos)
            shifted = moi.point_source_moi_at_elec_plane_shifted(charge_pos_shifted, elec_pos_shifted)
            self.assertAlmostEqual(orig, shifted, 6)

    def atest_line_source_formula_shifting(self):
        for n in xrange(100):
            h = np.random.random() * 500.
            set_up_parameters = {
                             'sigma_G': 0, # Below electrode
                             'sigma_T': np.random.random()*5.0, # Tissue
                             'sigma_S': np.random.random()*5.0, # Saline
                             'h': h, # um
                             'steps': 20,
                             }
            moi = MoI(**set_up_parameters)
            xpos_charge_0 = (np.random.random() - 0.5) * 100
            ypos_charge_0 = (np.random.random() - 0.5) * 100
            zpos_charge_0 = np.random.random() * h

            xpos_charge_1 = (np.random.random() - 0.5) * 100
            ypos_charge_1 = (np.random.random() - 0.5) * 100
            zpos_charge_1 = np.random.random() * h

            length = np.sqrt((xpos_charge_1 - xpos_charge_0)**2 +
                             (ypos_charge_1 - ypos_charge_0)**2 +
                             (zpos_charge_1 - zpos_charge_0)**2)

            xpos_elec = (np.random.random() - 0.5) * 100
            ypos_elec = (np.random.random() - 0.5) * 100
            zpos_elec = 0

            charge_pos_0 = [xpos_charge_0, ypos_charge_0, zpos_charge_0]
            charge_pos_1 = [xpos_charge_1, ypos_charge_1, zpos_charge_1]
            elec_pos = [xpos_elec, ypos_elec, zpos_elec]

            charge_pos_shifted_0 = [xpos_charge_0, ypos_charge_0, zpos_charge_0 - h / 2.]
            charge_pos_shifted_1 = [xpos_charge_1, ypos_charge_1, zpos_charge_1 - h / 2.]
            elec_pos_shifted = [xpos_elec, ypos_elec, -h / 2.]

            shifted = moi.line_source_moi_shifted(charge_pos_shifted_0, charge_pos_shifted_1, length, elec_pos_shifted)
            orig = moi.line_source_moi(charge_pos_0, charge_pos_1, length, elec_pos)
            self.assertAlmostEqual(orig, shifted, 6)

    def test_cython_LS_with_electrode(self):
        """ Test if cython extentions works as expected """

        set_up_parameters = {
            'sigma_G': 0.0, # Below electrode
            'sigma_T': 0.3, # Tissue
            'sigma_S': 0.3, # Saline
            'h': 200.,
            'steps': 20}

        xstart = np.array([100., 0, -100., -110])
        xend = np.array([100., 0, -100., -110]) + 100.

        ystart = np.array([100., 0, -100, 200.])
        yend = np.array([100., 0, -100, 200]) + 100.

        zstart = np.array([100, 70, 100, 50.])
        zend = np.array([100, 70, 100, 100]) + 10.

        elec_x = (np.arange(3) - 1)*50.
        elec_y = (np.arange(3) - 1)*50.

        #elec_x = np.array([0.])
        #elec_y = np.array([0.])

        elec_z = 0
        elec_r = 1
        n_avrg_points = 200

        ext_sim_dict = {'elec_x': elec_x,
                        'elec_y': elec_y,
                        'elec_z': elec_z,
                        'include_elec': True,
                        'use_line_source': True,
                        'moi_steps': set_up_parameters['steps'],
                        'n_avrg_points': n_avrg_points,
                        'elec_radius': elec_r
                        }
        moi = MoI(**set_up_parameters)
        t0 = time.time()
        mapping = LS_with_elec_mapping(set_up_parameters['sigma_T'],
                                       set_up_parameters['sigma_S'],
                                       set_up_parameters['h'], set_up_parameters['steps'],
                                       n_avrg_points, elec_r,
                                       elec_x, elec_y, xstart, ystart, zstart,
                                       xend, yend, zend)
        t_cy = time.time() - t0
        t0 = time.time()
        mapping2 = moi.make_mapping(ext_sim_dict, xstart=xstart, ystart=ystart, zstart=zstart,
                                            xend=xend, yend=yend, zend=zend)
        t_py = time.time() - t0
        rel_error = np.abs((mapping - mapping2)/mapping)
        print "\nLS with electrode cython speed-up: ", t_py/t_cy
        self.assertLessEqual(np.max(rel_error), 0.001)

    def test_cython_LS_without_electrode(self):
        """ Test if cython extentions works as expected """

        set_up_parameters = {
            'sigma_G': 0.0, # Below electrode
            'sigma_T': 0.3, # Tissue
            'sigma_S': 0.3, # Saline
            'h': 200,
            'steps': 20}

        xstart = np.array([100, 0, -100, -110.])
        xend = np.array([100, 0, -100, -110]) + 100.

        ystart = np.array([100, 0, -100, 200.])
        yend = np.array([100, 0, -100, 200.]) + 100

        zstart = np.array([10, 40, 10, 150.])
        zend = np.array([20, 1, 100, 50.])

        elec_x = (np.arange(3) - 1)*50.
        elec_y = (np.arange(3) - 1)*50.

        elec_z = 0

        ext_sim_dict = {'elec_x': elec_x,
                        'elec_y': elec_y,
                        'elec_z': elec_z,
                        'include_elec': False,
                        'use_line_source': True,
                        'moi_steps': set_up_parameters['steps'],
                        }
        moi = MoI(**set_up_parameters)
        t0 = time.time()
        mapping = LS_without_elec_mapping(set_up_parameters['sigma_T'],
                                          set_up_parameters['sigma_S'],
                                          set_up_parameters['h'], set_up_parameters['steps'],
                                          elec_x, elec_y, xstart, ystart, zstart,
                                          xend, yend, zend)
        t_cy = time.time() - t0
        t0 = time.time()
        mapping2 = moi.make_mapping(ext_sim_dict, xstart=xstart, ystart=ystart, zstart=zstart,
                                            xend=xend, yend=yend, zend=zend)
        t_py = time.time() - t0
        rel_error = np.abs((mapping - mapping2)/mapping)
        print "\nLS no electrode cython speed-up: ", t_py/t_cy
        self.assertLessEqual(np.max(rel_error), 0.001)

    def test_cython_PS_without_electrode(self):
        """ Test if cython extentions works as expected """

        set_up_parameters = {
            'sigma_G': 0.0, # Below electrode
            'sigma_T': 0.3, # Tissue
            'sigma_S': 0.3, # Saline
            'h': 200,
            'steps': 20}

        xmid = np.array([100, 0, -100.])
        ymid = np.array([100, 0, -100.])
        zmid = np.array([10, 4, 10.])

        elec_x = (np.arange(3) - 1)*50.
        elec_y = (np.arange(3) - 1)*50.
        elec_z = 0
        ext_sim_dict = {'elec_x': elec_x,
                        'elec_y': elec_y,
                        'elec_z': elec_z,
                        'include_elec': False,
                        'use_line_source': False,
                        'moi_steps': set_up_parameters['steps']
                        }
        moi = MoI(**set_up_parameters)
        t0 = time.time()
        mapping = PS_without_elec_mapping(set_up_parameters['sigma_T'],
                            set_up_parameters['sigma_S'],
                            set_up_parameters['h'], set_up_parameters['steps'],
                            elec_x, elec_y, xmid, ymid, zmid)
        t_cy = time.time() - t0
        t0 = time.time()
        mapping2 = moi.make_mapping(ext_sim_dict, xmid=xmid, ymid=ymid, zmid=zmid)
        t_py = time.time() - t0
        rel_error = np.abs((mapping - mapping2)/mapping)
        print "\nPS no electrode cython speed-up: ", t_py/t_cy
        self.assertAlmostEqual(np.max(rel_error), 0.0, 6)

    def test_cython_PS_with_electrode(self):
        """ Test if cython extentions works as expected """

        set_up_parameters = {
            'sigma_G': 0.0, # Below electrode
            'sigma_T': 0.3, # Tissue
            'sigma_S': 0.3, # Saline
            'h': 200,
            'steps': 20}

        xmid = 1000*np.array([0.1, 0, -0.1])
        ymid = 1000*np.array([0.1, 0, -0.1])
        zmid = 10*np.array([10.0, 10, 10.01])

        elec_x = (np.arange(3) - 1)*50.
        elec_y = (np.arange(3) - 1)*50.
        elec_z = 0
        elec_r = 1
        n_avrg_points = 100
        ext_sim_dict = {'elec_x': elec_x,
                        'elec_y': elec_y,
                        'elec_z': elec_z,
                        'include_elec': True,
                        'use_line_source': False,
                        'elec_radius': elec_r,
                        'moi_steps': set_up_parameters['steps'],
                        'n_avrg_points': n_avrg_points,
                        }
        moi = MoI(**set_up_parameters)

        t0 = time.time()
        mapping = PS_with_elec_mapping(set_up_parameters['sigma_T'], set_up_parameters['sigma_S'], set_up_parameters['h'],
                                       set_up_parameters['steps'], n_avrg_points, elec_r, elec_x, elec_y,
                                       xmid, ymid, zmid)
        t_cy = time.time() - t0
        t0 = time.time()
        mapping2 = moi.make_mapping(ext_sim_dict, xmid, ymid, zmid)
        t_py = time.time() - t0
        print "\nPS with electrode cython speed-up: ", t_py/t_cy
        rel_error = np.abs((mapping - mapping2)/mapping)
        self.assertLessEqual(np.max(rel_error), 0.001)

    def atest_cython_PS_without_electrode_shifting(self):
        """ Test if cython extentions works as expected """

        sigma_T = 0.3
        sigma_S = 0.3
        h = 200
        steps = 20

        xmid = np.array([100, 0, -100.])
        ymid = np.array([100, 0, -100.])
        zmid = np.array([10, 0, -10.])

        elec_x = (np.arange(3) - 1)*50.
        elec_y = (np.arange(3) - 1)*50.
        elec_z = -h/2.

        mapping_shifted = PS_without_elec_mapping_shifted(sigma_T, sigma_S, elec_z, steps, elec_x, elec_y, xmid, ymid, zmid)
        mapping = PS_without_elec_mapping(sigma_T, sigma_S, h,
                                          steps, elec_x, elec_y, xmid, ymid, zmid + h / 2.)
        rel_error = np.abs((mapping - mapping_shifted)/mapping)
        self.assertAlmostEqual(np.max(rel_error), 0.0, 6)

    def atest_cython_anis_PS_without_electrode_shifting(self):
        """ Test if cython extentions works as expected """

        h = 200
        steps = 20

        xmid = np.array([100, 0, -100.])
        ymid = np.array([100, 0, -100.])
        zmid = np.array([10, 0, -10.])

        elec_x = (np.arange(3) - 1)*50.
        elec_y = (np.arange(3) - 1)*50.
        elec_z = -h/2.

        sigma_T = 0.3
        sigma_S = 0.3
        with self.assertRaises(TypeError):
            mapping_shifted = anisotropic_PS_without_elec_mapping_shifted(elec_z, steps, sigma_T, sigma_S, elec_x, elec_y, xmid, ymid, zmid)
        with self.assertRaises(TypeError):
            mapping = anisotropic_PS_without_elec_mapping(sigma_T, sigma_S, h,
                                          steps, elec_x, elec_y, xmid, ymid, zmid + h / 2.)

        sigma_T = np.array([0.3, 0.1, 0.3])
        sigma_S = np.array([3., 3., 3.])
        mapping_shifted = anisotropic_PS_without_elec_mapping_shifted(elec_z, steps, sigma_T, sigma_S, elec_x, elec_y, xmid, ymid, zmid)
        mapping = anisotropic_PS_without_elec_mapping(h, steps, sigma_T, sigma_S, elec_x,
                                                                      elec_y, xmid, ymid, zmid + h / 2.)

        rel_error = np.abs((mapping - mapping_shifted)/mapping)
        self.assertAlmostEqual(np.max(rel_error), 0.0, 6)

    def atest_cython_PS_with_electrode_shifting(self):
        """ Test if cython extentions works as expected """

        sigma_T = 0.3
        sigma_S = 0.3
        h = 200
        steps = 20
        elec_r = 1.
        num_avrg_points = 100

        xmid = np.array([100, 0, -100.])
        ymid = np.array([100, 0, -100.])
        zmid = np.array([10, 0, -10.])

        elec_x = (np.arange(3) - 1)*50.
        elec_y = (np.arange(3) - 1)*50.
        elec_z = -h/2.

        mapping_shifted = PS_with_elec_mapping_shifted(sigma_T, sigma_S, elec_z, steps, num_avrg_points, elec_r, elec_x, elec_y, xmid, ymid, zmid)
        mapping = PS_with_elec_mapping(sigma_T, sigma_S, h,
                                          steps, num_avrg_points, elec_r, elec_x, elec_y, xmid, ymid, zmid + h / 2.)
        rel_error = np.abs((mapping - mapping_shifted)/mapping)
        self.assertAlmostEqual(np.max(rel_error), 0.0, 2)

    def atest_cython_LS_without_electrode_shifting(self):
        """ Test if cython extentions works as expected """

        sigma_T = 0.3
        sigma_S = 0.3
        h = 200
        steps = 20

        xstart = np.array([100, 5, -100.])
        ystart = np.array([100, 1, -100.])
        zstart = np.array([10, 8, -10.])

        xend = np.array([10, 0, -100.])
        yend = np.array([100, 0, -10.])
        zend = np.array([5, 0, -2.])

        elec_x = (np.arange(3) - 1)*50.
        elec_y = (np.arange(3) - 1)*50.
        elec_z = -h/2.

        mapping_shifted = LS_without_elec_mapping_shifted(sigma_T, sigma_S, elec_z, steps, elec_x, elec_y,
                                          xstart, ystart, zstart, xend, yend, zend)
        mapping = LS_without_elec_mapping(sigma_T, sigma_S, h, steps, elec_x, elec_y,
                                                          xstart, ystart, zstart + h / 2., xend, yend,
                                                          zend + h / 2.)
        rel_error = np.abs((mapping - mapping_shifted)/mapping)
        self.assertAlmostEqual(np.max(rel_error), 0.0, 6)

    def atest_cython_LS_with_electrode_shifting(self):
        """ Test if cython extentions works as expected """

        sigma_T = 0.3
        sigma_S = 3.0
        h = 200
        steps = 20
        elec_r = 1.2
        num_avrg_points = 100
        xstart = np.array([100, 5, -100.])
        ystart = np.array([100, 1, -100.])
        zstart = np.array([10, 8, -10.])

        xend = np.array([10, 0, -100.])
        yend = np.array([100, 0, -10.])
        zend = np.array([5, 0, -2.])

        elec_x = (np.arange(3) - 1)*50.
        elec_y = (np.arange(3) - 1)*50.
        elec_z = -h/2.

        mapping_shifted = LS_with_elec_mapping_shifted(sigma_T, sigma_S, elec_z, steps, num_avrg_points, elec_r, elec_x, elec_y,
                                          xstart, ystart, zstart, xend, yend, zend)
        mapping = LS_with_elec_mapping(sigma_T, sigma_S, h, steps, num_avrg_points,
                                                       elec_r, elec_x, elec_y, xstart, ystart,
                                                       zstart + h / 2., xend, yend,
                                                       zend + h / 2.)
        rel_error = np.abs((mapping - mapping_shifted)/mapping)
        self.assertAlmostEqual(np.max(rel_error), 0.0, 2)

if __name__ == '__main__':
    unittest.main()
