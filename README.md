Virtual MEA measurements in Python (ViMEAPy) is a Python toolbox for calculating the extracellular 
potential arising from a known current source distribution, in the in vitro slice setting, where a thin slice of neural tissue 
is immersed in saline on top of a Micro Electrode Array (MEA).
For this experimental set-up, calculating the extracellular potentials 
arising from a known distribution of current sources is a non-trivial problem because 
of the heterogeneous conductivity of the system. We provide here a Python toolbox that 
facilitates such modelling by use of the Method of Images.

For specific details, validations, and usage, please go to the 
article "Modelling and Analysis of Electrical Potentials Recorded in Microelectrode Arrays (MEAs)"
which is available here:
http://link.springer.com/article/10.1007%2Fs12021-015-9265-6

Note that this approach has also recently been implemented in LFPy2.0 (https://github.com/LFPy/LFPy), 
which provides easy usage of this approach in combination with neuronal simulations. 

For an example usage of ViMEAPy, run the main MoI.py file.

    Set-up:


              SALINE -> sigma_S = [sigma_Sx, sigma_Sy, sigma_Sz]

    <----------------------------------------------------> z = + h
    
              TISSUE -> sigma_T = [sigma_Tx, sigma_Ty, sigma_Tz]


                   o -> charge_pos = [x',y',z']


    <-----------*----------------------------------------> z = 0
                 \-> elec_pos = [x,y,z] 

                 ELECTRODE -> sigma_G = [sigma_Gx, sigma_Gy, sigma_Gz]